var dataImage = [
    {
        name: 'Versa',
        urls: [
            {
                color: 'pink',
                url: 'P_Fitbit/P_Versa_Pink.png',
            },
            {
                pinkOff: 'off',
                url: 'P_Fitbit/P_Versa_Pink_off.png',
            },
            {
                pinkOn: 'on',
                url: 'P_Fitbit/P_Versa_Pink_on.png'
            },
            {
                color: 'grey',
                url: 'P_Fitbit/P_Versa_Grey.png'
            },
            {
                color: 'black',
                url: 'P_Fitbit/P_Versa_Black.png'
            },
        ]
    },
    {
        name: 'Charge 3',
        urls: [
            {
            }
        ]
    },
    {
        name: 'HR',

    }
];